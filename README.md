# flexboxでこれまで難しかったレイアウトを楽にしてみる

![preview](img/img_preview.png)

よくありがちな、タイルレイアウトが並んでいるデザインだが、文字量が変動する場合、ボタンの位置を揃えるとか結構めんどくさい。

`display:flex;` を使用して比較的簡単に実装してみた。

See the Pen : [flexboxでこれまで難しかったレイアウトを楽にしてみる - 1](https://codepen.io/kazoo0217/pen/XgBZvy/)

