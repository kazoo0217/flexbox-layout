var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
//var autoprefixer = require('gulp-autoprefixer');
//

gulp.task('sass', function () {
  return gulp.src('./src/scss/*.scss')
    .pipe(sass({
			outputStyle: 'expanded'
			,indentType: 'tab'
			,indentWidth: 1
		}).on('error', sass.logError))
    .pipe(gulp.dest('./css/'));
});

gulp.task('sass:watch', function () {
  gulp.watch('./src/scss/*.scss', ['sass']);
});

// Static server
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "."
        }
    });
});

gulp.task('bs-reload', function () {
    browserSync.reload();
});

gulp.task('default', ['browser-sync','sass:watch'], function () {
    gulp.watch("./*.html", ['bs-reload']);
    gulp.watch("./css/*.css", ['bs-reload']);
});
